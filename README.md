  NaverWebtoonCrawler
========================

- Author: Jeong Minu ( last 3. 23. 2014 )
- support: minu.hanwool@gmail.com
- Python version: 2.7.6


Requirements
------------
#### Dependencies

- mechanize 0.2.5
> http://wwwsearch.sourceforge.net/mechanize/  
> usage: fake browsing.

- BeautifulSoup 4.3.2
> http://www.crummy.com/software/BeautifulSoup/  
> usage: parsing HTML document.
  
  -----
      Do not distribute obtained webtoon images.  
      No warranty about using code included,  
      but send me a mail freely if need help or advice, or want to give me an advice.  
      Every feedbacks will be welcomed.
  ---

#### READ_ME
- This code is tested only as a Starting point.  
- Will create a "crawler_log.txt" file. If you don't want, set return of SAVE_LOG() as False.  
- Will create folders with title of webtoons.  
- Do not remove comments before redistribution, especially READ_ME section.  
* Personal use only.  
* Redistribution of obtained webtoon images in public is ILLEGAL in most countries.  

#### Author test environment
- Sublime Text 3  
- Python 2.7.6  
- Mac OSX 64bit  

